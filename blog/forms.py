from django.forms import ModelForm
from django.forms import widgets
from .models import Opinion
from django import forms



class ContactFrom(ModelForm):
    class Meta:
        model = Opinion
    
        exclude = ['time']

        widgets = {
            'full_name': forms.TextInput(attrs={'class':'form-control', 'placeholder': 'الزامی', 'label':'نام کامل'}),
            'organization_name': forms.TextInput(attrs={'class':'form-control', 'placeholder': 'اختیاری', 'label':'نام ارگان یا شرکت'}),
            'responsibility': forms.TextInput(attrs={'class':'form-control', 'placeholder': 'اختیاری', 'label':'مسئولیت شما در سازمان'}),
            'email': forms.EmailInput(attrs={'class':'form-control', 'placeholder': 'الزامی', 'label':'ایمیل'}),
            'phone_number': forms.TextInput(attrs={'class':'form-control', 'placeholder': 'اختیاری', 'label':'شماره تلفن'}),
            'mobile_phone_number': forms.TextInput(attrs={'class':'form-control', 'placeholder': 'الزامی', 'label':'شماره موبایل'}),
            'description': forms.Textarea(attrs={'class':'form-control', 'placeholder': 'الزامی', 'label':'توضیحات'}),
        }
    
    def __init__(self, *args, **kwargs):
        super(ContactFrom, self).__init__(*args, **kwargs)
        self.fields['organization_name'].required = False