from django.db import models
from django.utils import timezone




class Blog(models.Model):
    title        = models.CharField(max_length=100)
    body         = models.TextField(max_length=1000)
    thumbnail    = models.ImageField(null=True, blank=True)
    created      = models.DateTimeField(auto_now_add = True)
    updated      = models.DateTimeField()


    def save(self, *args, **kwargs):
        if not self.created:
            self.created = timezone.now()

        self.updated = timezone.now()
        return super(Blog, self).save(*args, **kwargs)





class Opinion(models.Model):
    full_name           = models.CharField(max_length=50)
    organization_name   = models.CharField(max_length=50, null=True, blank=True,default="وارد نشده")
    responsibility      = models.CharField(max_length=50, null=True, blank=True, default="وارد نشده")
    email               = models.EmailField()
    phone_number        = models.CharField(null=True, blank=True,max_length=12)
    mobile_phone_number = models.CharField(max_length=12)
    description         = models.TextField(max_length = 500)
    time                = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-time']

    def __str__(self) -> str:
        return self.email




