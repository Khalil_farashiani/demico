from django.urls import path
from .views import (
                        laningpage,
                        about_us,
                        custumer,
                        daronchahi,
                        daronchahi2,
                        licens,
                        main,
                        organizationconstruction,
                        policyguidline,
                        process,
                        productAnchor,
                        productLockMandrel,
                        productPacker,
                        products,
                        productSafetyValve,
                        productSSD,
                        productTravel,
                        strategyandgoals,
                        contact_email,
                        liciso1,
                        liciso2,
                        liciso3,
                        liciso4,
                        liciso5,
                        liciso6,
                        hafari,
                        licenseiso,
                        licensentebagh,
                        licensetaeidie,
                        licensesaier,
                        productHanalyse,
                        productHctu,
                        productHfoam,
                        productHmot,
                        productHnitrogengenerator,
                        lavazemBottomNoGo,
                        lavazemFlowCouplin,
                        lavazemFlutedCasing,
                        lavazemFlutedSwedge,
                        lavazemMillOut,
                        lavazemMillOutExtension,
                        lavazemPerforated,
                        lavazemTopNoGo,
                        lavazemTubingCross,
                        lavazemyadaki,
                        p2product
                    )





app_name = 'blog'

urlpatterns = [
    path('', laningpage, name=''),
    path('aboutus/', about_us, name='aboutus'),
    path('custumer/', custumer, name='customer'),
    path('daronchahi/', daronchahi, name='daronchahi'),
    path('daronchahi2/', daronchahi2, name='daronchahi2'),
    path('hafari/', hafari, name='hafari'),
    
    path('licens/', licens, name='licens'),
    path('licenseiso/' , licenseiso, name='licenseiso'),
    path('licensentebagh/', licensentebagh, name='licenseentebagh'),
    path('licensetaeidie/', licensetaeidie, name='licensetaeidie'),
    path('licensesaier/', licensesaier, name='licesnsesaier'),
    
    path('main/', main, name='main'),
    # path('news/', news, name='news'),
    path('organizationconstruction/', organizationconstruction, name='organizationconstruction'),
    path('policyguidline/', policyguidline, name='policyguidline'),
    path('process/', process, name='process'),
    path('productAnchor/', productAnchor, name='productAnchor'),

    path('productHanalyse/', productHanalyse, name='productHanalyse'),
    path('productHctu/', productHctu, name='productHctu'),
    path('productHfoam/', productHfoam, name='prodcutHfoam'),
    path('productHmot/', productHmot, name='prodcutHmot'),
    path('productHnitrogengenerator/', productHnitrogengenerator, name='productHnitrogeangenerator'),

    path('productLockMandrel/', productLockMandrel, name='productLockMandrel'),
    path('productPacker/', productPacker, name='productPacker'),
    path('products/', products, name='products'),
    path('productSafetyValve/', productSafetyValve, name='productSafetyValve'),
    path('productSSD/', productSSD, name='productSSD'),
    path('productTravel/', productTravel, name='productTravel'),
    path('strategyandgoals/', strategyandgoals, name='strategyandgoals'),
    path('liciso1/', liciso1, name='liciso1'),
    path('liciso2/', liciso2, name='liciso2'),
    path('liciso3/', liciso3, name='liciso3'),
    path('liciso4/', liciso4, name='liciso4'),
    path('liciso5/', liciso5, name='liciso5'),
    path('liciso6/', liciso6, name='liciso6'),


    path('lavazemBottomNoGo/', lavazemBottomNoGo, name='lavazemBottomNoGo'),
    path('lavazemFlowCouplin/', lavazemFlowCouplin, name='lavazemFlowCouplin'),
    path('lavazemFlutedCasing/', lavazemFlutedCasing, name='lavazemFlutedCasing'),
    path('lavazemFlutedSwedge/', lavazemFlutedSwedge, name='lavazemFlutedSwedge'),
    path('lavazemMillOut/', lavazemMillOut, name='lavazemMillOut'),
    path('lavazemMillOutExtension/', lavazemMillOutExtension, name='lavazemMillOutExtension'),
    path('lavazemPerforated/', lavazemPerforated, name='lavazemPerforated'),
    path('lavazemTopNoGo/', lavazemTopNoGo, name='lavazemTopNoGo'),
    path('lavazemTubingCross/', lavazemTubingCross, name='lavazemTubingCross'),
    path('lavazemyadaki/', lavazemyadaki, name='lavazemyadaki'),

    path('p2product/', p2product, name='p-2product'),



    path('contact/', contact_email, name='email')
]
