from django.shortcuts import redirect, render
from django.contrib import messages
from .models import Blog
from .forms import ContactFrom


def laningpage(requset):
    return redirect('blog:main')


def about_us(requset):
    return render(requset, 'blog/aboutus.html')



def custumer(requset):
    return render(requset, 'blog/customer.html')



def daronchahi(requset):
    return render(requset, 'blog/daronchahi.html')



def daronchahi2(requset):
    return render(requset, 'blog/daronchahi2.html')



def licens(requset):
    return render(requset, 'blog/licens.html')


def main(requset):
    news = Blog.objects.all()[:5]
    return render(requset, 'blog/main.html', {'news': news})


def managment(requset):
    return render(requset, 'blog/managment.html')


def managmentNews(requset):
    return render(requset, 'blog/managmentNews.html')


def navtest(requset):
    return render(requset, 'blog/navtest.html')


def news(requset):
    news = Blog.objects.all()[:5]
    return render(requset, 'blog/news.html', {'news': news})


def organizationconstruction(requset):
    return render(requset, 'blog/organizationconstruction.html')


def policyguidline(requset):
    return render(requset, 'blog/policyguidline.html')


def process(requset):
    return render(requset, 'blog/process.html')


def productAnchor(requset):
    return render(requset, 'blog/productAnchor.html')


def productLockMandrel(requset):
    return render(requset, 'blog/productLockMandrel.html')


def productPacker(requset):
    return render(requset, 'blog/productPacker.html')


def products(requset):
    return render(requset, 'blog/products.html')


def productSafetyValve(requset):
    return render(requset, 'blog/productSafetyValve.html')


def productSSD(requset):
    return render(requset, 'blog/productSSD.html')



def productTravel(requset):
    return render(requset, 'blog/productTravel.html')



def strategyandgoals(requset):
    return render(requset, 'blog/strategy&goals.html')



def liciso1(requset):
    return render(requset, 'blog/liciso1.html')


def liciso2(requset):
    return render(requset, 'blog/liciso2.html')


def liciso3(requset):
    return render(requset, 'blog/liciso3.html')


def liciso4(requset):
    return render(requset, 'blog/liciso4.html')


def liciso5(requset):
    return render(requset, 'blog/liciso5.html')


def liciso6(requset):
    return render(requset, 'blog/liciso6.html')


def hafari(requset):
    return render(requset, 'blog/hafari.html')



def licenseiso(requset):
    return render(requset, 'blog/licenseiso.html')

def licensentebagh(requset):
    return render(requset, 'blog/licensentebagh.html')

def licensetaeidie(requset):
    return render(requset, 'blog/licensetaeidie.html')

def licensesaier(requset):
    return render(requset, 'blog/licesnsesaier.html')               




def productHanalyse(request):
    return render(request, 'blog/productHanalyse.html')

def productHctu(request):
    return render(request, 'blog/productHctu.html')

def productHfoam(request):
    return render(request, 'blog/productHfoam.html')



def productHmot(request):
    return render(request, 'blog/productHmot.html')



def productHnitrogengenerator(request):
    return render(request, 'blog/productHnitrogengenerator.html')


def lavazemBottomNoGo(request):
    return render(request, 'blog/lavazemBottomNoGo.html')



def lavazemFlowCouplin(request):
    return render(request, 'blog/lavazemFlowCouplin.html')



def lavazemFlutedCasing(request):
    return render(request, 'blog/lavazemFlutedCasing.html')


def lavazemFlutedSwedge(request):
    return render(request, 'blog/lavazemFlutedSwedge.html')


def lavazemMillOut(request):
    return render(request, 'blog/lavazemMillOut.html')


def lavazemMillOutExtension(request):
    return render(request, 'blog/lavazemMillOutExtension.html')


def lavazemPerforated(request):
    return render(request, 'blog/lavazemPerforated.html')


def lavazemTopNoGo(request):
    return render(request, 'blog/lavazemTopNoGo.html')


def lavazemTubingCross(request):
    return render(request, 'blog/lavazemTubingCross.html')


def lavazemyadaki(request):
    return render(request, 'blog/lavazemyadaki.html')


def p2product(request):
    return render(request, 'blog/p-2product.html')


def contact_email(request):
    if request.method == 'POST':
        form = ContactFrom(request.POST)
        if form.is_valid():
            post = form.save()
            messages.success(request, 'دیدگاه شما ثبت شد')
            return redirect('blog:main')
    
    else:
        form = ContactFrom(request.POST)
    return render(request, 'blog/email.html', {'form': form})
