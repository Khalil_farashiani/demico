window.addEventListener('scroll', function () {
  let header = document.getElementById('header');
  let logo = document.getElementById('demico-logo');
  logo.classList.toggle('logo-apear', window.screenY > 0);
  header.classList.toggle('header-scrolled', window.scrollY > 0);
})